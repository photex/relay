package relay.example

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Success, Failure}
import akka.actor.{ActorSystem, ActorRef, Actor, ActorLogging, Props}
import akka.util.Timeout
import akka.pattern.ask
import relay._

class DemoActor(manager: ActorRef) extends Actor with ActorLogging {
  val BYE = "!bye".r
  val WHOIS = "!whois (.+)".r

  override def postStop() {
    sys.exit(0)
  }
  
  def receive = {
    case ChannelReady(name) =>
      log.info(s"CHANNEL READY RECEIVED for $name")
      val channel = sender
      channel ! "Hello from a relay demonstration!"
      context become {
        case PrivMsg(from, content) => content match {
          case BYE() =>
            channel ! s"ok $from, see ya later."
            channel ! Part("I was asked to leave.")
            manager ! Quit
            context stop self
          case WHOIS(url) =>
            channel ! s"$from: I'd like to but I don't know how to do that yet."
          case _ => log.debug(content)
        }
      }
  }
}

object Demo {
  implicit val timeout = Timeout(5.seconds)
  def main(args: Array[String]) {
    val system = ActorSystem("relay-demo")
    implicit val ec = system.dispatcher

    val manager = system.actorOf(Manager(), "manager")
    val demo_handler = system.actorOf(Props(classOf[DemoActor], manager), "demo")

    val f = manager.ask(OpenNetwork("oftc", "irc.oftc.net", 6667)).mapTo[NetworkOpened]
    f onComplete {
      case Success(opened) =>
        val oftc = opened.network
        oftc ! User("relay", "Relay Demo")
        oftc ! Nick("relay-demo")
        oftc ! Join("#relay-demo", demo_handler)
        sys.addShutdownHook {
          system.shutdown()
        }
      case Failure(_) =>
        system.shutdown()
    }
  }
}











