package relay

import akka.actor.{Actor, ActorRef, ActorLogging, Props}
import akka.util.ByteString


object Channel {
  def apply(name: String, handler: ActorRef): Props = Props(classOf[Channel], name, handler)
}

class Channel(name: String, handler: ActorRef) extends Actor with ActorLogging {

  override def preStart() {
    handler ! ChannelReady(name)
  }

  def receive = {
    case Part(reason) =>
      context.parent ! ByteString(s"PART $name :$reason\r\n")
      context.stop(self)

    case msg: PrivMsg => handler.forward(msg)

    case message: String =>
      context.parent ! ByteString.fromString(s"PRIVMSG $name :$message\r\n")

    case msg => log.info(msg.toString)
  }
}
