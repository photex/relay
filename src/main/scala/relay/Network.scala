package relay

import java.net.InetSocketAddress
import akka.actor.{Actor, ActorRef, ActorLogging, Props, Stash}
import akka.io.{IO, Tcp}
import akka.util.ByteString


object Network {
  def apply(name: String, server: InetSocketAddress): Props =
    Props(classOf[Network], name, server)

  object Matchers {
    val PING = "PING :*(.+)\r*\n$".r
    val NOTICE = ":.+ NOTICE (.+)\r*\n$".r
    val PRIVMSG = ":(.+)!.+ PRIVMSG (.+) :(.+)\r*\n$".r
  }
}

class Network(name: String, server_addr: InetSocketAddress) extends Actor with ActorLogging with Stash {
  import Tcp._
  import context.system
  import Network.Matchers._

  IO(Tcp) ! Connect(server_addr)

  def receive = disconnected

  def disconnected: Receive = {
    case CommandFailed(_: Connect) =>
      context.parent ! NetworkError(name, "Unable to connect")
      context.stop(self)

    case Connected(remote, local) =>
      log.info(s"Connected to ${server_addr.toString}")
      sender ! Register(self)
      unstashAll()
      context.become( connected(sender, Map.empty[String, ActorRef]) )

    case msg: IrcCommand => stash()
  }

  def connected(connection: ActorRef, channels: Map[String, ActorRef]): Receive = {
    case User(username, realname) =>
      connection ! Write(ByteString.fromString(s"USER $username 0 * :$realname\r\n", "utf8"))

    case Nick(nick) =>
      connection ! Write(ByteString.fromString(s"NICK $nick\r\n", "utf8"))

    case Pass(password) =>
      connection ! Write(ByteString.fromString(s"PASS $password\r\n", "utf8"))

    case Quit(reason) =>
      connection ! Write(ByteString.fromString(s"QUIT :$reason\r\n", "utf8"))
      context.parent ! NetworkClosed(name)
      context.stop(self)

    case Join(channelname, handler) =>
      val actor_name = channelname.replace('#', '@')
      val channel = context.actorOf(Channel(channelname, handler), actor_name)
      connection ! Write(ByteString.fromString(s"JOIN :$channelname\r\n", "utf8"))
      context.become( connected(connection, channels + (channelname -> channel)) )

    case Received(data) =>
      val msg = data.utf8String
      msg match {
        case PING(source) =>
          connection ! Write(ByteString.fromString(s"PONG :$source\r\n", "utf8"))

        case NOTICE(content) => log.debug(content)

        case PRIVMSG(from, to, content) =>
          channels.get(to).foreach { channel =>
            channel ! PrivMsg(from, content)
          }
          log.debug(s"$from in $to > $content")

        case msg => log.debug(msg)
      }

    case _: ConnectionClosed =>
      log.debug(s"Connection to $name was closed.")
      context.parent ! NetworkClosed(name)
      context.stop(self)

    case data: ByteString => connection ! Write(data)
  }

}
