import akka.actor.ActorRef

package object relay {

  sealed trait RelayMessage

  final case class OpenNetwork(name: String, host: String, port: Int) extends RelayMessage
  final case class NetworkOpened(network: ActorRef) extends RelayMessage
  final case class NetworkClosed(name: String) extends RelayMessage
  final case class NetworkError(name: String, message: String) extends RelayMessage
  final case class ChannelReady(name: String) extends RelayMessage

  sealed trait IrcCommand extends RelayMessage

  final case class User(username: String, realname: String) extends IrcCommand
  final case class Nick(nick: String) extends IrcCommand
  final case class Join(channel: String, handler: ActorRef) extends IrcCommand
  final case class Part(reason: String) extends IrcCommand
  final case class Pass(password: String) extends IrcCommand
  final case class PrivMsg(from: String, message: String) extends IrcCommand
  final case class Quit(reason: String) extends IrcCommand
  
}
