package relay

import java.net.InetSocketAddress
import akka.actor.{Actor, ActorLogging, Props, ActorRef}


object Manager {
  def apply(): Props = Props[Manager]
}

class Manager extends Actor with ActorLogging {

  def receive = managing(Map.empty[String, ActorRef])

  def managing(networks: Map[String, ActorRef]): Receive = {
    case OpenNetwork(name, host, port) =>
      log.info(s"Request to open network $name received")
      val address = new InetSocketAddress(host, port)
      val network = context.actorOf(Network(name, address), name)
      sender ! NetworkOpened(network)
      context.become( managing(networks + (name -> network)) )

    case NetworkClosed(name) =>
      log.info(s"Network '$name' was closed.")
      context.become( managing(networks - name) )

    case NetworkError(name, message) =>
      log.error(s"Error: $name - $message")
      context.become( managing(networks - name) )

    case msg: Quit =>
      networks.values.foreach(_ ! msg)
  }
  
}
