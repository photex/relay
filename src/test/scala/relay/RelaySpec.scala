package com.example

import org.scalatest.{WordSpec, MustMatchers}

class RelaySpec extends WordSpec with MustMatchers {

  "foo" must {
    "bar" in {
      "foo bar" must equal ("foo bar")
    }
  }

}
